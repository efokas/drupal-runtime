<?php
$attributemap = array(
	    'http://schemas.xmlsoap.org/claims/EmailAddress'	=> 'email',
		'http://schemas.xmlsoap.org/claims/CommonName'	=> 'login',
		'http://schemas.xmlsoap.org/claims/Group'	=> 'egroups',
		'http://schemas.xmlsoap.org/claims/DisplayName'	=> 'fullname',
		'http://schemas.xmlsoap.org/claims/PhoneNumber'	=> 'phonenumber',
		'http://schemas.xmlsoap.org/claims/Building'	=> 'building',
		'http://schemas.xmlsoap.org/claims/Firstname'	=> 'firstname',
		'http://schemas.xmlsoap.org/claims/Lastname'	=> 'lastname',
		'http://schemas.xmlsoap.org/claims/Department'	=> 'department',
	    'http://schemas.xmlsoap.org/claims/HomeInstitute'	=> 'homeinstitute',
		'http://schemas.xmlsoap.org/claims/PersonID'	=> 'personid',
	    'http://schemas.xmlsoap.org/claims/PreferredLanguage'	=> 'preferredlanguage',
	    'http://schemas.xmlsoap.org/claims/IdentityClass'	=> 'identityclass',
);