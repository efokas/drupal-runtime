# This Dockerfile builds the *sitebuilder-base* image that deploys the CERN Drupal distribution
# and serves as a basis for all Drupal websites.

ARG PHP_BASE_VERSION
FROM gitlab-registry.cern.ch/drupal/paas/drupal-runtime/php-base:$PHP_BASE_VERSION

# DRUPAL_VERSION is passed by Gitlab as an argument through branch name/ tag name
ARG DRUPAL_VERSION

ENV DRUPAL_VERSION=${DRUPAL_VERSION}

LABEL io.openshift.s2i.scripts-url="image:///usr/libexec/s2i" \
      io.s2i.scripts-url="image:///usr/libexec/s2i" \
      io.k8s.description="Drupal Site Builder s2i build" \
      io.k8s.display-name="Drupal Site Builder" \
      io.openshift.tags="builder,drupal" \
      maintainer="Drupal Admins <drupal-admins@cern.ch>"

COPY ./s2i/bin/ ./hooks/ /usr/libexec/s2i/
RUN chmod -R +x /usr/libexec/s2i/

ADD ./configuration/sitebuilder/fix-permissions /fix-permissions
RUN chmod +x /fix-permissions

# DRUPAL
# Path configuration
# Set up drupal site folder
RUN mkdir -p /app

ENV DRUPAL_APP_DIR /app

# Prepare folder to create necessary folders from composer
# Clone the CERN Drupal distribution at the appropriate version (git tag)
RUN git clone --depth=1 --branch=${DRUPAL_VERSION} https://gitlab.cern.ch/drupal/paas/composer-drupal-project.git ${DRUPAL_APP_DIR}

WORKDIR ${DRUPAL_APP_DIR}

RUN mkdir ${DRUPAL_APP_DIR}/.composer
RUN /fix-permissions ${DRUPAL_APP_DIR}/.composer
ENV COMPOSER_HOME=${DRUPAL_APP_DIR} COMPOSER_CACHE_DIR=${DRUPAL_APP_DIR}/.composer

# Do not run Composer as root/super user! See https://getcomposer.org/root for details
# Set up drupal minimum stack
ENV COMPOSER_MEMORY_LIMIT=-1
RUN composer install --optimize-autoloader -vvv

# Clean-up composer installation
# Rename composer.json to composer.admins.json so that when user injects its composer,
# there is no conflict between then.
RUN cp ${DRUPAL_APP_DIR}/composer.json ${DRUPAL_APP_DIR}/composer.admins.json

# Add extra configurations
# At this point, composer has created the required settings.php through:
# post-update-cmd: DrupalProject\composer\ScriptHandler::createRequiredFiles
# Overwrite settings.php with ours.
# - settings.php
ADD ./configuration/sitebuilder/settings.php ${DRUPAL_APP_DIR}/web/sites/default/settings.php
# Remove ${DRUPAL_APP_DIR}/web/sites/default/{files, private, modules, themes}, preparing it to be symbolic link after init-app.sh;
RUN rm -rf ${DRUPAL_APP_DIR}/web/sites/default/files
RUN rm -rf ${DRUPAL_APP_DIR}/web/sites/default/private
RUN rm -rf ${DRUPAL_APP_DIR}/web/sites/default/modules
RUN rm -rf ${DRUPAL_APP_DIR}/web/sites/default/themes

# Explicity create the site configuration dir as configured in settings-d8.php#L17 file
RUN mkdir -p config/sync
# Fix-permissions
RUN /fix-permissions ${DRUPAL_APP_DIR}

CMD ["/usr/libexec/s2i/usage"]
