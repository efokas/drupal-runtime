# Drupal Runtime

This repository contains the source-to-image recipes, Dockerfiles and other configuration required for the runtime environment of the Drupal infrastructure,
orchestrated by the [Drupal Site Operator](https://gitlab.cern.ch/drupal/paas/drupalsite-operator/)

The images in this repo are the sources of BuildConfigs that generate the actual runtime images for every Drupal site
based also on extra configuration injected by the users.

## Architecture

![Architecture Diagram](https://gitlab.cern.ch/paas-tools/okd4-install/-/raw/master/docs/cluster-flavours/drupal/drupal-design.svg)

Ref: https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/docs/cluster-flavours/drupal

## CI & Registry

Drupal runtime repo builds three images for every new **tag** and hosts the three images in the repo registry.

!!! note
    To trigger the build of new image tags, we need to push a _git tag_ to this repo (see next section).

The three images are 
- php-base
    - Refers to [images/php-base/Dockerfile](./images/php-base/Dockerfile)
    - Based on upstream PHP and installs the required components
- nginx-base
    - Doesn't have a Dockerfile specifically. Upstream image is pulled and tagged on the fly in the [Gitlab CI](./.gitlab-ci.yml#L27). 
    - Based on upstream Nginx
- site-builder-base
    - Refers to [Dockerfile](./Dockerfile)
    - Based on the `php-base` image, copies the s2i scripts and also builds the basic drupal project cloned from [cern-drupal-distribution](https://gitlab.cern.ch/drupal/paas/composer-drupal-project)

![Pipeline](./pipeline.png)

### Creating new artifacts
To run a new CI pipeline, create a new tag with the 'Drupal version' as the name. Make sure the tag name matches with the corresponding branch in [cern-drupal-distribution](https://gitlab.cern.ch/drupal/paas/composer-drupal-project)

All the three images built, will be tagged with the same Drupal version.

## Configuring Versions
Versions of different components referred in the Dockerfile are configured using [config.yaml](./config.yaml)

The same file can be used to manage versions for a new dependency in the future.

Given this, we are explicitly specifying versions for dependencies in the `config.yaml` file for a **given Drupal Version** 

### Build arguments and CI variables

The following table lists the build arguments and variables used in the Dockerfiles and the CI.

| Variable | Description | 
| ----------- | ----------- |
| COMPOSER_VERSION | Controls the composer version to be installed |
| DRUPAL_VERSION | Controls the drupal version to be used |
| NGINX_VERSION | Controls the nginx image version to be pulled |
| PHP_VERSION | Controls the php image version to be pulled |
| DRUSH_VERSION | Controls the drush version to be used |
| DRUPAL_APP_DIR | The directory that hosts the drupal code |
| DRUPAL_SHARED_VOLUME | Mount path of persistent volume |
| SITE_BUILDER_DIR | This directory points to the location, where the drupal code will be available to be copied from in the Openshift buildConfig |
| CI_COMMIT_REF_NAME | Gitlab tag/ branch name |
